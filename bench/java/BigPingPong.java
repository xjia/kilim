import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class BigPingPong extends Thread {

    static class Msg {
        int from;
        Msg(int f) { from = f; }
    }

    static class Mailbox {

        Msg[] a;
        int p, q;
        Semaphore s;

        Mailbox(int capacity) {
            a = new Msg[capacity];
            p = q = 0;
            s = new Semaphore(0);
        }

        void put(Msg m) {
            synchronized(this) { a[q++] = m; }
            s.release();
        }

        Msg get() {
            s.acquireUninterruptibly();
            Msg m = null;
            synchronized(this) { m = a[p++]; }
            return m;
        }

        Msg getb(long timeout) throws InterruptedException {
            Msg m = null;
            if (s.tryAcquire(timeout, TimeUnit.MILLISECONDS)) {
                synchronized(this) { m = a[p++]; }
            }
            return m;
        }
    }

    static Mailbox mainmb;
    static Mailbox[] mboxes;

    int me;
    BigPingPong(int num) { me = num; }

    boolean done = false;
    int  numRcvd = 0;

    public void run() {
        done = false;

        Msg mymsg = new Msg(me);
        Mailbox mymb = mboxes[me];

        for (int i = 0; i < mboxes.length; i++) {
            if (i == me) continue;
            mboxes[i].put(mymsg);
        }
        for (int i = 0; i < mboxes.length - 1; i++) {
            Msg m = mymb.get();
            assert m != null;
            numRcvd++;
        }

        mainmb.put(mymsg);
        done = true;
    }

    public static void main(String[] args) throws InterruptedException {
        boolean noargs = args.length == 0;
        int nTasks = noargs ? 10 : Integer.valueOf(args[0]);

        for (int c = 0; c < 10; c++) {
            long beginTime = System.currentTimeMillis();

            mainmb = new Mailbox(nTasks);
            mboxes = new Mailbox[nTasks];
            for (int i = 0; i < nTasks; i++) {
                mboxes[i] = new Mailbox(nTasks);
            }

            BigPingPong[] tasks = new BigPingPong[nTasks];
            for (int i = 0; i < nTasks; i++) {
                BigPingPong t = new BigPingPong(i);
                tasks[i] = t;
                t.start();
            }

            for (int i = 0; i < nTasks; i++) {
                Msg m = mainmb.getb(20000);
                if (m == null) {
                    System.err.println("TIME OUT (20s). No of tasks finished: " + i);
                    System.exit(1);
                }
            }

            System.out.println(System.currentTimeMillis() - beginTime);

            System.gc();
            Thread.sleep(1000);
        }
    }
}
